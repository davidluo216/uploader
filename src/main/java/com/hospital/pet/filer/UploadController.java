package com.hospital.pet.filer;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class UploadController {

    @PostMapping("upload")
    public String upload(MultipartFile file, String directory){
        try {
            return FileUtil.upload(file, directory);
        }catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    @GetMapping("getFileByte")
    public byte[] getFileContent(String fileURl){
        return FileUtil.getFileBytes(fileURl);
    }

    @GetMapping("deleteFile")
    public String deleteF(String fileURl){
        return FileUtil.deleteFile(fileURl);
    }


    HashMap<String, String[] > chunkMap = new HashMap<>();

    @PostMapping("uploadChunk")
    public String uploadChunk(String fileId,MultipartFile chunk,int chunkNum,int totalNum,boolean isVideo) throws InterruptedException, IOException {
        if(chunkNum == 0) {
            if (chunkMap.containsKey(fileId)) {
                return "fail,该field已存在！请更换fieldId";
            }
            String[] arr = new String[totalNum];
            for (int i = 0; i < totalNum; i++) {
                arr[i] = "";
            }
        }

        int cnt = 0;
        while (!chunkMap.containsKey(fileId)){
            Thread.sleep(500);
            cnt++;
            if(cnt>10){
                return "fail,wait too long to have fileId key";
            }
        }

        if(chunkNum!=totalNum-1){
            String url = upload(chunk,"chunk");
            chunkMap.get(fileId)[chunkNum]=url;
            return "success";
        }else{
            //开始合并
            cnt = 0;
            for(int i=0;i<totalNum;i++){
                while (chunkMap.get(fileId)[i].equals("")){
                    Thread.sleep(500);
                    cnt++;
                    if(cnt>150){
                        return "fail,wait too long to upload other chunks";
                    }
                }
            }

            String dir;
            if(isVideo){
                dir = "medical_case/video";
            }else{
                dir = "medical_case/image";
            }

            String url = FileUtil.genUrl(chunk,dir);
            File file = new File(url);
            file.createNewFile();
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            for(int i=0;i<totalNum-1;i++){
                bos.write(getFileContent(chunkMap.get(fileId)[i]));
                bos.flush();
            }
            bos.write(chunk.getBytes());
            bos.flush();
            bos.close();
            chunkMap.remove(fileId);
            return url;
        }
    }
}
