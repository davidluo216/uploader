package com.hospital.pet.filer;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.UUID;

@Service
public class FileUtil {

    private static String uploadPath;

    private static String server;

    private static String urlPrefix;

    private static String URL_PREFIX;

    public FileUtil(Environment environment){
        uploadPath=environment.getProperty("upload.path");
        server=environment.getProperty("upload.server");
        urlPrefix=environment.getProperty("upload.url.prefix");
        URL_PREFIX="http://"+server+"/"+urlPrefix+"/";
    }

    public static String upload(MultipartFile file, String directory) throws IOException {
        String filename=file.getOriginalFilename();
        String extension=filename==null ? "" : filename.substring(filename.lastIndexOf("."));
        // 以UUID重命名文件
        String storeFilename=UUID.randomUUID().toString()+extension;
        File storedFile=new File(uploadPath+directory+'/'+storeFilename);
        file.transferTo(storedFile);
        return splicingPath(storeFilename, directory);
    }

    public static String genUrl(MultipartFile file, String directory){
        String filename=file.getOriginalFilename();
        String extension=filename==null ? "" : filename.substring(filename.lastIndexOf("."));
        // 以UUID重命名文件
        String storeFilename=UUID.randomUUID().toString()+extension;
        return splicingPath(storeFilename, directory);
    }

    public static String deleteFile(String fileURI){ ;

        String relativePath = fileURI.substring(fileURI.indexOf(URL_PREFIX)+URL_PREFIX.length());

        String Path = uploadPath+relativePath;
        File file = new File(Path);
        if(!file.exists()){
            return "文件不存在";
        }else if(file.delete()){
            return "删除成功";
        }else{
            return "删除失败";
        }
    }

    public static byte[] getFileBytes(String fileURI){
        String relativePath = fileURI.substring(fileURI.indexOf(URL_PREFIX)+URL_PREFIX.length());

        String Path = uploadPath+relativePath;
        File file = new File(Path);
        if(!file.exists()){
            return null;
        }else {
            return fileToByte(file);
        }
    }

    private static byte[] fileToByte(File file) {
        ByteArrayOutputStream baos=  new ByteArrayOutputStream((int)file.length());
        BufferedInputStream bis = null;

        try {
            bis = new BufferedInputStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int buf_size = 1024;
        byte[] buffer = new byte[buf_size];
        int len =0;
        while(true){
            try {
                assert bis != null;
                if (-1 == (len = bis.read(buffer, 0, buf_size))) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            baos.write(buffer,0,len);
        }
        return baos.toByteArray();
    }



    /**
     * 拼接文件url
     * @param filename 文件名
     * @param directory 目录
     * @return 文件url
     */
    public static String splicingPath(String filename, String directory){
        return URL_PREFIX+directory+'/'+filename;
    }
}
